import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Todo } from '../list-to-do/list-to-do.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  id: number;
  todo: Todo;

  constructor(
    private todoService: TodoDataService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.todo = new Todo(this.id, '', new Date(), false);
    if (this.id != -1) {
      this.todoService.retrieveTodo('damian', this.id)
        .subscribe(
          response => this.todo = response
        )
    }
  }

  saveToDo() {
    console.log(this.id);
    console.log(this.id == -1);
    if (this.id == -1) {
      console.log('create new todo');
      //Create todo
      this.todoService.createToDo('damian', this.todo)
        .subscribe(
          response => {
            this.router.navigate(['todos'])
          }
        )
    } else {
      console.log('update existing todo');
      this.todoService.updateToDo('damian', this.id, this.todo)
        .subscribe(
          response => {
            this.router.navigate(['todos'])
          }
        )
    }
  }

}
