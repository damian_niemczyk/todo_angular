import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL } from 'src/app/app.constants';

export class HelloWorldBean {
  constructor(public msg:string) {
    
  }
}

@Injectable({
  providedIn: 'root'
})
export class WelcomeDataService {

  constructor(
    private http:HttpClient
  ) { }

  executeHelloWorldBeanService() {
    return this.http.get<HelloWorldBean>(`${API_URL}/hello-world-bean`);
  }

  executeHelloWorldBeanServiceWithPathVariable(name) {
    // let basicAuthenticationString = this.createBasicAuthenticationHttpHeader();
    
    // let header = new HttpHeaders({
    //   Authorization: basicAuthenticationString
    // })

    return this.http.get<HelloWorldBean>(`${API_URL}/hello-world-path-variable/${name}`/*{headers: header}*/);
  }

  // createBasicAuthenticationHttpHeader() {
  //   let username = "damian"
  //   let password = "password"
  //   let basicAuthString = "Basic " + window.btoa(username + ":" + password)
  //   return basicAuthString;
  // }
}
