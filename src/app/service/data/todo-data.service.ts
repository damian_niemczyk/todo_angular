import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from 'src/app/list-to-do/list-to-do.component';
//import { API_URL } from 'src/app/app.constants';
import { JPA_API_URL } from 'src/app/app.constants';

@Injectable({
  providedIn: 'root'
})

export class TodoDataService {

  constructor(private http:HttpClient) { }

  retrieveAllTodos(username: string) {
    return this.http.get<Todo[]>(`${JPA_API_URL}/users/${username}/todos`);
  }

  retrieveTodo(username: string, id: number) {
    return this.http.get<Todo>(`${JPA_API_URL}/users/${username}/todos/${id}`);
  }

  deleteToDo(username: string, id: number) {
    return this.http.delete(`${JPA_API_URL}/users/${username}/todos/${id}`);
  }

  updateToDo(username: string, id: number, todo) {
    return this.http.put(`${JPA_API_URL}/users/${username}/todos/${id}`, todo);
  }

  createToDo(username: string, todo) {
    return this.http.post(`${JPA_API_URL}/users/${username}/todos`, todo);
  }
}
