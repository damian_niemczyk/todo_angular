import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Router } from '@angular/router';

export class Todo {
  constructor(
    public id: number,
    public description: string,   
    public targetDate: Date,
    public done: boolean
  ) {

  }
}

@Component({
  selector: 'app-list-to-do',
  templateUrl: './list-to-do.component.html',
  styleUrls: ['./list-to-do.component.css']
})
export class ListToDoComponent implements OnInit {

  todos: Todo[];

  message: string;

  /*todos = [
    new Todo(1, 'learn to dance', new Date(), false),
    new Todo(1, 'learn to html', new Date(), false),
    new Todo(1, 'learn to css', new Date(), false)
    //{ id: 1, description: 'learn to dance' },
    //{ id: 2, description: 'learn to html' },
    //{ id: 3, description: 'learn to css' }
  ]*/

  /*todo = {
    id: 1,
    description: 'learn to dance'
  }*/

  constructor(
    private service: TodoDataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.refreshToDos();
  }

  private refreshToDos() {
    this.service.retrieveAllTodos("damian").subscribe(response => {
      console.log(response);
      this.todos = response;
    });
  }

  deleteToDo(id) {
    this.service.deleteToDo("damian", id).subscribe(
      response => {
        console.log(response);
        this.message = `Successful delete of ${id}`;
        this.refreshToDos();
      }
    );
  }

  updateTodo(id) {
    this.router.navigate(['todos', id]);
  }

  addTodo() {
    this.router.navigate(['todos', -1])
  }

}
