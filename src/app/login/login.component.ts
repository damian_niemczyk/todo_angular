import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HardcodedAuthenticationService } from '../service/hardcoded-authentication.service';
import { BasicAuthenticationService } from '../service/basic-authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = "damian";
  password = "password";
  errorMessage = 'Invalid credentails';
  invalidlogin = false;

  //Router passed via dependency injection
  constructor(private router: Router,
    private hardcodedAuthenticationService: HardcodedAuthenticationService,
    private basicAuthenticationService: BasicAuthenticationService
    ) {}

  ngOnInit() {
  }

  /*handleLogin() {
    console.log(this.username);
    if (this.hardcodedAuthenticationService.autheniticate(this.username, this.password)) {
      //Redirect to welcome page
      this.router.navigate(['welcome', this.username]);
      this.invalidlogin = false;
    } else {
      this.invalidlogin = true;
    }
  }*/

  handleBasicAuthLogin() {
    this.basicAuthenticationService.executeAuthenticationService(this.username, this.password)
      .subscribe(
        data => {
          this.router.navigate(['welcome', this.username])
          this.invalidlogin = false;
        },
        error => {
          this.invalidlogin = true;
        }
      )
  }

  handleJWTAuthLogin() {
    this.basicAuthenticationService.executeJWTAuthenticationService(this.username, this.password)
      .subscribe(
        data => {
          this.router.navigate(['welcome', this.username])
          this.invalidlogin = false;
        },
        error => {
          this.invalidlogin = true;
        }
      )
  }

}
